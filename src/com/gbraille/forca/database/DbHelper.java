package com.gbraille.forca.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
	public static String DATABASE_NAME = "gbrailleForca.db";
	private static int DATABASE_VERSION = 1;
	
	public static String TABLE_RESPOSTAS = "respostas";
	public static String TABLE_DIFICULDADE = "dificuldade";
	public static String TABLE_PONTUACAO = "pontuacao";
	public static String TABLE_INSTALACAO = "instalacao";
	
	/* table dificuldade */
	public static String COLUMN_DIFICULDADE = "dificuldade";
	
	/* table respostas */
	public static String COLUMN_ID = "_id";	
	public static String COLUMN_PERGUNTA = "pergunta";
	public static String COLUMN_RESPOSTA = "resposta";	
	public static String COLUMN_LETRA_FALTA_POS = "letrafaltapos";
	public static String COLUMN_ACESSADO = "acessado";
	
	/* table pontuacao */
	public static String COLUMN_PONTUACAO = "pontuacao";
	
	/* table instalacao */
	public static String COLUMN_INSTALADO = "instalado";
		
	private static String CREATE_TABLE_INSTALACAO = "create table " + TABLE_INSTALACAO 
			+ "( " 
			+ COLUMN_ID + " INTEGER PRIMARY KEY, "
			+ COLUMN_INSTALADO + " VARCHAR(255) "
			+ ");";
	
	private static String CREATE_TABLE_DIFICULDADE = "create table " + TABLE_DIFICULDADE 
			+ "( " 
			+ COLUMN_ID + " INTEGER PRIMARY KEY, "
			+ COLUMN_DIFICULDADE + " VARCHAR(255) "
			+ ");";
	
	private static String CREATE_TABLE_RESPOSTAS = "create table " + TABLE_RESPOSTAS 
			+ "( " 
			+ COLUMN_ID + " INTEGER PRIMARY KEY, "
			+ COLUMN_PERGUNTA + " VARCHAR(255), "
			+ COLUMN_RESPOSTA + " VARCHAR(255), "
			+ COLUMN_LETRA_FALTA_POS + " VARCHAR(2), "
			+ COLUMN_DIFICULDADE + " INTEGER REFERENCES " + TABLE_DIFICULDADE + "(" + COLUMN_ID + "), "
			+ COLUMN_ACESSADO + " VARCHAR(1) "
			+ ");";
	
	private static final String INSERT_INSTALACAO_NAO = "insert into " 
		    + TABLE_INSTALACAO + " (" 
		    + COLUMN_ID + ", "		         
		    + COLUMN_INSTALADO + ") values (1,'N')";
	
	
	private static final String INSERT_DIFICULDADE_FACIL = "insert into " 
		    + TABLE_DIFICULDADE + " (" 
		    + COLUMN_ID + ", "		         
		    + COLUMN_DIFICULDADE + ") values (1,'facil')";
	
	private static final String INSERT_DIFICULDADE_DIFICIL = "insert into " 
		    + TABLE_DIFICULDADE + " (" 
		    + COLUMN_ID + ", "		         
		    + COLUMN_DIFICULDADE + ") values (2,'dificil')";
	
	private static String CREATE_TABLE_PONTUACAO = "create table " + TABLE_PONTUACAO 
			+ "( " 
			+ COLUMN_ID + " INTEGER PRIMARY KEY, "
			+ COLUMN_DIFICULDADE + " INTEGER REFERENCES " + TABLE_DIFICULDADE + "(" + COLUMN_ID + "), "
			+ COLUMN_PONTUACAO + " INTEGER "
			+ ");";
	
	private static final String INSERT_PONTUACAO_FACIL = "insert into " 
		    + TABLE_PONTUACAO + " (" 
		    + COLUMN_ID + ", "
		    + COLUMN_DIFICULDADE + ", "
		    + COLUMN_PONTUACAO + ") values (1,1,0)";
	
	private static final String INSERT_PONTUACAO_DIFICIL = "insert into " 
		    + TABLE_PONTUACAO + " (" 
		    + COLUMN_ID + ", "
		    + COLUMN_DIFICULDADE + ", "
		    + COLUMN_PONTUACAO + ") values (2,2,0)";
	
	public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_DIFICULDADE);
		db.execSQL(INSERT_DIFICULDADE_FACIL);
		db.execSQL(INSERT_DIFICULDADE_DIFICIL);		
		db.execSQL(CREATE_TABLE_RESPOSTAS);	
		db.execSQL(CREATE_TABLE_PONTUACAO);
		db.execSQL(INSERT_PONTUACAO_FACIL);
		db.execSQL(INSERT_PONTUACAO_DIFICIL);
		db.execSQL(CREATE_TABLE_INSTALACAO);
		db.execSQL(INSERT_INSTALACAO_NAO);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 Log.i(DbHelper.class.getName(),"Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIFICULDADE);
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESPOSTAS);
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_PONTUACAO);
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_INSTALACAO);
		 onCreate(db);
	}
	
	public SQLiteDatabase getDatabase() {
	    return this.getWritableDatabase();
	}
}